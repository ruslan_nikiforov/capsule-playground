import React from 'react';
import App, { Container } from 'next/app';

import Head from '../components/head';
import Header from '../components/Header';
import Footer from '../components/Footer';

import '../static/styles/index.scss';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <Container>
        <Head title="vi+amin_d" />
        <Header/>
        <Component {...pageProps} />
        <Footer/>
      </Container>
    )
  }
}

export default MyApp;