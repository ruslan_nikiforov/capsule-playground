import React from 'react'
import Link from 'next/link'
import { withRouter } from 'next/router'

import ContactForm from '../components/ContactForm'
import FullWidthVideo from '../components/FullWidthVideo'

import './style.scss';

const Home = (props = {}) => {
  const { router = {} } = props;
  const { query = {}} = router;

  return (
    <div>
      <FullWidthVideo
        url="https://player.vimeo.com/video/331250183?autoplay=1&amp;title=0&amp;byline=0&amp;portrait=0"
      />
      <div className="section-title-wrapper">
        <h1 className="section-title">
          Our clients aren't the <br/>
          only exciting things  <br/>
          we're working on.
        </h1>
      </div>
      <ContactForm isSubmitted={query.contact_submitted}/>
    </div>
  );
}

export default withRouter(Home)
