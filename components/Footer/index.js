import React from 'react';

import './style.scss';

const Footer = () => {

  return (
    <div className="contacts-block-wrapper">
      <div className="contacts-block">
        <h3 className="contacts-block-title">
          If you would like more information on what we do,<br/>
          please get in touch at:
        </h3>
        <div className="contacts">
            <a href="mailto:checkup@vitaminlondon.com">checkup@vitaminlondon.com</a>
            <a href="tel:+44 207 233 6423">+44 207 233 6423</a>
            <span>47 Gillingham Street Westminster SW1V1HS</span>
        </div>
        <div className="socials">
            <a href="https://www.instagram.com/vitaminlondon/?hl=en" target="_blank" className="social instagram-wrapper">
                <div className="icon instagram"/>
                <div className="icon icon-hovered instagram-hovered"/>
            </a>
            <a href="https://www.facebook.com/vitaminlondon/" target="_blank" className="social facebook-wrapper">
                <div className="icon facebook"/>
                <div className="icon icon-hovered facebook-hovered"/>
            </a>
            <a href="https://twitter.com/vitaminlondon" target="_blank" className="social twitter-wrapper">
                <div className="icon twitter"/>
                <div className="icon icon-hovered twitter-hovered"/>
            </a>
        </div>
      </div>
    </div>
  );
}

export default Footer;