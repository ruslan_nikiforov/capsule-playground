import React from 'react';

import './style.scss';

const FullWidthVideo = (props = {}) => {
  const {url = ''} = props;

  return (
    <div className="videoWrapper">
      <iframe
        src={url}
        width="640"
        height="260"
        frameBorder="0"
        webkitallowfullscreen=""
        mozallowfullscreen=""
        allowFullScreen=""/>
    </div>
  )
};

export default FullWidthVideo;