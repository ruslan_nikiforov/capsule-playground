import React from 'react';

import './style.scss';

const Header = props => (
  <header className="header">
    <img src="static/img/logo.svg" alt="Fancy logo" className="logo"/>
  </header>
)

export default Header
