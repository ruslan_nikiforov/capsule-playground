import React, {Fragment, useState, useEffect} from 'react';
import TextareaAutosize from 'react-autosize-textarea';

import './style.scss';

const statusToMessage = {
  '': '',
  'Loading': (
    <Fragment>
      <span>Loading... </span>
      <img src="static/img/santa-walk.gif"/>
    </Fragment>
  ),
  'Ok': (
    <Fragment>
      <span>We will contact you soon!</span>
      <img src="static/img/santa_ride.png"/>
    </Fragment>
  ),
  'Fail': (
    <Fragment>
      <span>Something went wrong.</span>
      <img src="static/img/dead_dear.png"/>
    </Fragment>
  ),
};

const sendContactForm = async (data = {}, setStatus, status) => {
  if (status === 'Loading') {
    return;
  }

  setStatus('Loading');
  fetch(location.href + 'contact-form',
    {
      method: 'POST',
      body:  JSON.stringify(data),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }
  )
  .then(res => res.json())
  .then(json => {
    console.log('json.status: ', json.status);
    setStatus(json.status);
  })
  .catch(err => { throw err});
} 

export default () => {
  const [status, setStatus] = useState('');
  const [name, setName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [description, setDescription] = useState('');
  const message = statusToMessage[status];
  
  useEffect(() => {
    if (status === 'Ok') {
      setName('');
      setEmail('');
      setLastName('');
      setDescription('');
    }

    if (status === 'Fail') {
      setTimeout(() => setStatus(''), 5000);
    }
  }, [status])
  
  return (
    <div className="contact-form-wrapper">
      <div className={`contact-form ${status === 'Ok' ? 'completed' : ''}`} method="post" name="contact_form">
        <div className="contact-form-top">
          <div className="form-item">
            <p className="form-item-label"> First name: </p>
            <input
              type="text"
              name="FIRST_NAME"
              value={name}
              onChange={e => setName(e.target.value)}
            />
          </div>
          <div className="form-item">
            <p className="form-item-label"> Last name: </p> 
            <input
              type="text"
              name="LAST_NAME"
              value={lastName}
              onChange={e => setLastName(e.target.value)}
            />
          </div>
          <div className="form-item">
            <p className="form-item-label"> Email: </p> 
            <input
              type="email"
              name="EMAIL1"
              value={email}
              onChange={e => setEmail(e.target.value)}
            />
          </div>
        </div>
        <div className="contact-form-describe">
          <TextareaAutosize
            rows={3}
            value={description}
            onChange={e => setDescription(e.target.value)}
            placeholder="Describe your idea"
            className="contact-form-description"
          />
        </div>
        <div
          className="cool-button"
          onClick={() => sendContactForm({name, lastName, email, description}, setStatus, status)}
        >
          <div className="svg-wrapper">
            <svg height="40" width="150" xmlns="http://www.w3.org/2000/svg">
              <rect id="shape" height="40" width="150"/>
            </svg>
          </div>
          <button className="button-text">
            Contact
          </button>
        </div>
      </div>
      <p className="respond">
        {message}
      </p>
    </div>
  );
}