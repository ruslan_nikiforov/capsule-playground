const express = require('express')
const next = require('next')
const bodyParser = require('body-parser')
const fetch = require('node-fetch');
const FormData = require('form-data');

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()

  const jsonParser = bodyParser.json();

  server.post('/contact-form', jsonParser, async (req, res) => {
    const data = req.body;

    const details = {
      'FIRST_NAME': data.name,
      'LAST_NAME': data.lastName,
      'EMAIL': data.email,
      'FORM_ID': '15498ff0-7693-429f-a2c9-3f52d339ad17'
    };
    
    var formBody = [];
    for (var property in details) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    let result = false;

    // Post contact form
    await fetch('https://service.capsulecrm.com/service/newlead', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: formBody
    })
    .then(res => result = res.status === 200)
    .catch(err => { console.error(err); throw err; });

    let party = {};
    // Create new party with user data from contact form
    await fetch('https://api.capsulecrm.com/api/v2/parties', {
      method: 'POST',
      headers: {
        'Authorization': 'Bearer /wGdyoheGPCV9cu3ZjB2dKont4WUmJWsYLsk+YjHHg63XTvty8o8nsqpHNSnPgPF',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "party": {
          "type": "person",
          "firstName": data.name,
          "lastName": data.lastName,
          "emailAddresses": [
            {
              "type": "Work",
              "address": data.email
            }
          ]
        }
      })
    })
    .then(res => { 
      console.log('res: ', res);
      return res.status === 201 ? res.json() : result = false;
    })
    .then(data => party = data.party)
    .catch(err => {throw err});

    if (result) {
      // Create new opportunity with Contact milestone and created user (party)
      await fetch('https://api.capsulecrm.com/api/v2/opportunities', {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer /wGdyoheGPCV9cu3ZjB2dKont4WUmJWsYLsk+YjHHg63XTvty8o8nsqpHNSnPgPF',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          opportunity: {
            name: "New contact",
            description: data.description,
            party: {id: party.id},
            milestone: {id:1578335}
          }
        })
      })
      .then(res => {
        console.log('result: ', res);
        return result = res.status === 201;
      })
      .catch(err => console.error(err));
    }

    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({ status: result ? 'Ok' : 'Fail' }));
  })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})